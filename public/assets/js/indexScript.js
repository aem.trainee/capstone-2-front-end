let navItems = document.querySelector('#navSession')
console.log(navItems)

let userToken = localStorage.getItem("token")
console.log(userToken)

if(!userToken){

    navItems.innerHTML = 
    `
    <li class="nav-item">
        <a href="./index.html" class="nav-link">Home</a>
    </li>
    <li class="nav-item">
        <a href="./pages/courses.html" class="nav-link">Courses</a>
    </li>
    <li class="nav-item">
    <a href="./pages/register.html" class="nav-link"> Register </a>
</li>
    <li class="nav-item">
        <a href="./pages/login.html" class="nav-link">Log in</a>
    </li>
    `
    document.querySelector('#hideNoToken').remove()

}else{
    navItems.innerHTML =  
    `
    <li class="nav-item">
    <a href="./index.html" class="nav-link">Home</a>
    </li>
    <li class="nav-item">
        <a href="./pages/courses.html" class="nav-link">Courses</a>
    </li>
    <li class="nav-item">
    <a href="./pages/profile.html" class="nav-link">Profile</a>
    </li>
    <li class="nav-item">
    <a href="./pages/logout.html" class="nav-link">Log Out</a>
    </li>
    `
}
