let token = localStorage.getItem('token')
let updateId = localStorage.getItem('updateThis')

fetch(`https://aem-booking-system.onrender.com/api/courses/${updateId}`, {
    method: 'GET',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }
})
.then(res => res.json())
.then( data => {
    document.querySelector('#courseName').value = data.name
    document.querySelector('#coursePrice').value = data.price
    document.querySelector('#courseDescription').value = data.description

   


document.querySelector('#editCourse').addEventListener('submit', (e, data) => {
    e.preventDefault()
    let name = document.querySelector('#courseName').value
    let description = document.querySelector('#courseDescription').value 
    let price = parseInt(document.querySelector('#coursePrice').value)
        let updatedData = {
            "courseId" : localStorage.getItem('updateThis'),
            "name" : name,
            "description" : description,
            "price" : price
        }

        fetch('https://aem-booking-system.onrender.com/api/courses', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(updatedData)
        })
        .then(res => res.json())
        .then( data => {
            Qual.successdb('Success', 'Course Updated')
                let delayInMilliseconds = 2000; //2 second
                setTimeout(function() {
                    window.location.replace('./courses.html')
                }, delayInMilliseconds);
        })
    })
})