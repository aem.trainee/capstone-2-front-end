let specificCourseId = localStorage.getItem('specificCourseId')
let token = localStorage.getItem('token')

if(token){
    fetch(`https://aem-booking-system.onrender.com/api/courses/${specificCourseId}`, {
    method: "GET",
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    }
    })
    .then(res => res.json())
    .then(data => {
        let courseNameDisplay = data.name
        document.querySelector('#courseName').innerHTML = data.name
        document.querySelector('#courseDesc').innerHTML = data.description
        document.querySelector('#coursePrice').innerHTML = data.price
        document.querySelector('#enrollContainer').innerHTML = 
         `<button class = 'customButton1 col-12 w-75' id='enroll-${data._id}'>Enroll <i class="fas fa-folder-plus"></i></button>`

        document.querySelector(`#enroll-${data._id}`).addEventListener('click', function(){
            fetch(`https://aem-booking-system.onrender.com/api/users/checkEnrolledCourse`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json', 
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({"courseId": specificCourseId})
            })
            .then(res => res.json())
            .then( data => {
                console.log(data)
                if(data == false){
                    fetch(`https://aem-booking-system.onrender.com/api/users/enroll`, {
                        method: "POST",
                        headers: {
                            'Content-Type': 'application/json', 
                            'Authorization': `Bearer ${token}`
                        },
                        body: JSON.stringify({"courseId": specificCourseId})
                    })
                    .then(res => res.json())
                    .then(data => {
                        if(data == true){
                            Qual.successdb('Success', `Enrolled to ${courseNameDisplay}`)
                            let delayInMilliseconds = 2000; //2 second
                            setTimeout(function() {
                                window.location.replace('./courses.html')
                            }, delayInMilliseconds);
                        }
                    })
                }else{
                    Qual.errordb('Error', `You are already enrolled to this course`)
                }
            })
                
        })

    })
}else{
    fetch(`https://aem-booking-system.onrender.com/api/courses/${specificCourseId}/nonAdmin`, {
    method: "GET",
    headers: {
        'Content-Type': 'application/json',
    }
    })
    .then(res => res.json())
    .then(data => {
        let courseNameDisplay = data.name
        document.querySelector('#courseName').innerHTML = data.name
        document.querySelector('#courseDesc').innerHTML = data.description
        document.querySelector('#coursePrice').innerHTML = data.price
        document.querySelector('#enrollContainer').innerHTML = 
        `<a href="./courses.html" class = 'customButton1 col-12'>back <i class="fas fa-undo-alt"></i></a>`
    })

}
