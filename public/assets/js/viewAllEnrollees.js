let token = localStorage.getItem('token')
let courseId = localStorage.getItem('courseEnrolleesView')
let allData = []

fetch(`https://aem-booking-system.onrender.com/api/courses/${courseId}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(res => res.json())
    .then(data => {
        let students = []
        data.enrollees.forEach(index => {
            students.push({studentId: index.userId})
        })
        
        document.querySelector('#courseName').innerHTML = data.name
        
        students.forEach(index => {
            fetch(`https://aem-booking-system.onrender.com/api/users/getStudentName`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
                body: JSON.stringify({"_id": index.studentId})
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                allData.push({
                    _id: data._id,
                    name: `${data.firstName} ${data.lastName}`,
                    email: data.email,
                    mobileNo: data.mobileNo,
                    enrollment: data.enrollment
                })
                console.log(allData)
                // $('#allEnrollees').append(` 
                // <tr>
                // <td class="color-white">${data.firstName} ${data.lastName}</td>
                // <td class="color-white">${data.email}</td>
                // <td class="color-white">${data.mobileNo}</td>
                // </tr>`)
                if(allData.length == students.length)
                {
                    allData.sort(function(a, b){
                        var x = a.name.toLowerCase();
                        var y = b.name.toLowerCase();
                        if (x < y) {return -1;}
                        if (x > y) {return 1;}
                        return 0;
                    });
                    allData.forEach( index => {
                        let status
                        index.enrollment.forEach( i => {
                            if(i.courseId == courseId){
                                 status = i.status
                            }
                        })
                        $('#allEnrollees').append(` 
                        <tr>
                        <td class="color-white">${index.name}</td>
                        <td class="color-white">${index.email}</td>
                        <td class="color-white">${index.mobileNo}</td>
                        <td class="color-white">${status}</td>
                        <td class="color-white"><button id="drop-${index._id}" class='customButton6'>Enroll/Drop</button></td>
                        </tr>`)

                        $(`#drop-${index._id}`).click(function (){
                
                            fetch(`https://aem-booking-system.onrender.com/api/users/change`, {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Authorization': `Bearer ${token}`
                                },
                                body: JSON.stringify({"_id": index._id, "courseId": courseId})
                            })
                            .then(res => res.json())
                            .then( data => {
                                if(data.message == 'status changed'){
                                    Qual.successdb('Success', 'Status Changed')
                                    let delayInMilliseconds = 2000; //2 second
                                    setTimeout(function() {
                                        window.location.replace('./viewAllEnrollees.html')
                                    }, delayInMilliseconds);
                                }
                            })
                       
                        })
                            
                    })


                }
            })
        })
        

    })
