let adminUser = localStorage.getItem("isAdmin")
let token = localStorage.getItem("token")
let modalButton = document.querySelector("#adminButton")
let cardFooter

if(adminUser == 'false' || !adminUser){
    modalButton.innerHTML = null
}else{
    modalButton.innerHTML = 
    `
    <div class="col-12 text-right mt-5 mb-3">
	    <a href="./addCourse.html" class="customButton2">Add Course <i class="fas fa-plus-circle"></i></a>
    </div>
    `
}

if(token){
    fetch('https://aem-booking-system.onrender.com/api/courses/all',{
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        }
    })
    .then( res => res.json())
    .then( data => {
        let courseData;

        if(data.length < 1){
            courseData = 'No courses available'
            let container = document.querySelector('#coursesContainer')
            container.innerHTML = courseData
        }else{
            if(adminUser == 'true'){
                courseData = data.map(course => {
                    let date = new Date(course.createdOn)
                    finalDate = date.getFullYear()+'-' + (date.getMonth()+1) + '-'+date.getDate();
                    return(
                        `
                        <div class="col-md-6 my-3">
                            <div class="card glassPure">
                                <div class="card-body">
                                    <h5 class="card-title color-white">${course.name}<span id="status-${course._id}" class="float-right"></span></h5>
                                    
                                    <p class="card-text text-left color-white">
                                        ${course.description}
                                    </p>
                                    <p class="card-text text-right color-white">
                                        P${course.price}
                                    </p>
                                    
                                    <p class="card-text text-left color-white">
                                        Created On: ${finalDate}
                                    </p>
                                </div>
                                <div class="card-footer">
                                <button class = 'customButton3 col-12' id='view-${course._id}'>View Enrollees <i class="fas fa-eye"></i></button>
                                <button class = 'customButton4 col-12 mt-2' id='update-${course._id}'>Update <i class="far fa-edit"></i></button>
                                <button class = 'customButton5 col-12 mt-2' id='delete-${course._id}'>Enable/Disable <i class="fas fa-sync-alt"></i></button>
                                </div>
                            </div>
                        </div>
                        `
                    )
                }).join("")
                //since the collection is an array, use join to separate the 
                let container = document.querySelector('#coursesContainer')
                container.innerHTML = courseData
        
                data.forEach(index => {
                    //delete button
                    if(index.isActive == true){
                        document.querySelector(`#status-${index._id}`).innerHTML = `<img src="https://img.icons8.com/fluent/48/000000/ok.png"/>`
                    }else if(index.isActive == false){
                        document.querySelector(`#status-${index._id}`).innerHTML = `<img src="https://img.icons8.com/emoji/48/000000/cross-mark-emoji.png"/>`
                    }
                    document.querySelector(`#delete-${index._id}`).addEventListener('click', function(){
                        fetch(`https://aem-booking-system.onrender.com/api/courses/${index._id}`, {
                            method: 'DELETE',
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization': `Bearer ${token}`
                            }
                        })
                        .then(res => res.json())
                        .then (data => {
                            if(data == true){
                                localStorage.setItem('courseDeleted', 'true')
                                window.location.replace('./deleteCourse.html')
                            }
                        })
                    })

                    //update button
                    document.querySelector(`#update-${index._id}`).addEventListener('click', function(){
                        localStorage.setItem('updateThis', index._id)
                        window.location.replace('./editCourse.html')
                    })

                    //view enrollees button
                    document.querySelector(`#view-${index._id}`).addEventListener('click', function(){
                        localStorage.setItem('courseEnrolleesView', index._id)
                        window.location.replace('./viewAllEnrollees.html')
                    })
                    
                })
            }else{
                
                let activeArray = []
                data.forEach(index => {
                    if(index.isActive == true){
                    activeArray.push(index)
                    }
                })
                courseData = activeArray.map(course => {
                    let date = new Date(course.createdOn)
                    finalDate = date.getFullYear()+'-' + (date.getMonth()+1) + '-'+date.getDate();
                    return(
                        `
                        <div class="col-md-6 my-3">
                            <div class="card glassPure">
                                <div class="card-body">
                                    <h5 class="card-title color-white">${course.name}</h5>
                                    <p class="card-text text-left color-white">
                                        ${course.description}
                                    </p>
                                    <p class="card-text text-right color-white">
                                        P${course.price}
                                    </p>
                                    <p class="card-text text-left color-white">
                                        Created on: ${finalDate}
                                    </p>
                                </div>
                                <div class="card-footer">
                                <button class = 'customButton5 col-12' id='view-${course._id}'>Enroll <i class="fas fa-folder-plus"></i></button>
                                </div>
                            </div>	
                        </div>
                        `
                    )
                }).join("")
                //since the collection is an array, use join to separate the 
                let container = document.querySelector('#coursesContainer')
                container.innerHTML = courseData

                activeArray.forEach(index => {
                    document.querySelector(`#view-${index._id}`).addEventListener('click', function(){
                        localStorage.setItem('specificCourseId', index._id)
                        window.location.replace('./course.html')
                    })
                })

            }
        }
    })
}else{
    fetch('https://aem-booking-system.onrender.com/api/courses/',{
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        }
    })
    .then( res => res.json())
    .then(data => {
        let activeArray = []
                data.forEach(index => {
                    if(index.isActive == true){
                    activeArray.push(index)
                    }
                })

        courseData = activeArray.map(course => {
            let date = new Date(course.createdOn)
            finalDate = date.getFullYear()+'-' + (date.getMonth()+1) + '-'+date.getDate();
            return(
                `
                <div class="col-md-6 my-3">
                    <div class="card glassPure">
                        <div class="card-body">
                            <h5 class="card-title color-white">${course.name}</h5>
                            <p class="card-text text-left color-white">
                                ${course.description}
                            </p>
                            <p class="card-text text-right color-white">
                                P${course.price}
                            </p>
                            <p class="card-text text-left color-white">
                                Created on: ${finalDate}
                            </p>
                            <p class="card-text text-left" id="status-${course._id}">
                                
                            </p>
                            <div class="">
                                <button class = 'customButton5 col-12' id='view-${course._id}'>View <i class="fas fa-eye"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                `
            )
        }).join("")
            let container = document.querySelector('#coursesContainer')
            container.innerHTML = courseData

            activeArray.forEach(index => {
                document.querySelector(`#view-${index._id}`).addEventListener('click', function(){
                    localStorage.setItem('specificCourseId', index._id)
                    window.location.replace('./course.html')
                })
            })
    })
}



