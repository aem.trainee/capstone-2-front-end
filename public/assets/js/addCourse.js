let addCourseForm = document.querySelector('#createCourse')
let token = localStorage.getItem('token')

addCourseForm.addEventListener('submit',(e) => {
    e.preventDefault()
    let courseName = document.querySelector('#courseName').value
    let coursePrice = document.querySelector('#coursePrice').value
    let intPrice = parseInt(coursePrice)
    let courseDescription = document.querySelector('#courseDescription').value
    let gatheredInfo = {
        name: courseName,
        description: courseDescription,
        price: intPrice
    }
    if(courseName == '' || coursePrice == '' || courseDescription == '' ){
        Qual.errordb('Error', 'Fill up all the fields');
    }else{
        fetch('https://aem-booking-system.onrender.com/api/courses', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(gatheredInfo)
        })
        .then(res => res.json())
        .then(data => {
            if(data == true){
                
                Qual.successdb('Success', 'Course added');
                let delayInMilliseconds = 2000; //1 second

                setTimeout(function() {
                //your code to be executed after 1 second
                window.location.replace('./courses.html')
                }, delayInMilliseconds);
                
                // alert('COURSE SUCCESSFULLY ADDED')
                
            }else{
                alert('COURSE NOT SUCCESSFULLY ADDED')
            }
        })
    }
})