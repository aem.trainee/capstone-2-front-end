let token = localStorage.getItem('token')
let isAdmin = localStorage.getItem('isAdmin')
let user = localStorage.getItem('id')
let variable
let allData = []

if(token){
    fetch('https://aem-booking-system.onrender.com/api/users/details', {
        method: "GET",
        headers: {
            'Content-Type': 'application/json', 
            'Authorization': `Bearer ${token}`
        }
    })
    .then(res => res.json())
    .then(data => {
        if(isAdmin == 'false'){
            document.querySelector('#profileContainer').innerHTML = 
            `<div class="col-12 text-center glassPure mt-5 mb-5">
                <h2 class="color-white mt-5">ENROLLED COURSES</h2>
                <h5 class="mt-3 color-white">Name: ${data.firstName} ${data.lastName}</h5>
                <h5 class="color-white">Email: ${data.email}</h5>
                <h5 class="color-white">Mobile Number: ${data.mobileNo}</h5>
                <div>
                    <div style="overflow-x:auto;">
                    <table class="table mt-5 mb-5" id="enrollments">
                        <tr> 
                            <th class="color-white">Course Name</th>
                            <th class="color-white">Description</th>
                            <th class="color-white">Enrolled On</th>
                            <th class="color-white">Status</th>
                            <th class="color-white">Price</th>
                        </tr>
                    </table>
                    <div>
                </div>
            </div>`

            $('#allStudents').remove()
            let length = data.enrollment.length
            data.enrollment.forEach(index => {
                fetch(`https://aem-booking-system.onrender.com/api/courses/${index.courseId}`, {
                        method:'GET',
                        headers: {
                            'Content-Type': 'application/json', 
                            'Authorization': `Bearer ${token}`
                        }
                }) 
                .then(res => res.json())
                .then(data => {

                    data.enrollees.forEach(index => {
                        if(index.userId == user){
                            variable = index.enrolledOn
                        }
                    })

                    let date = new Date(variable);
                    date = date.getFullYear()+'-' + (date.getMonth()+1) + '-'+date.getDate();

                    allData.push({
                        courseName: data.name,
                        description: data.description,
                        enrolledOn: date,
                        status: index.status,
                        price: data.price
                    })

                    if(allData.length == length){
                        allData.sort(function(a, b){
                            var x = a.courseName.toLowerCase();
                            var y = b.courseName.toLowerCase();
                            if (x < y) {return -1;}
                            if (x > y) {return 1;}
                            return 0;
                        });
                        allData.forEach( index => {
                            $('#enrollments').append(
                            `
                            <tr><td class="color-white">${index.courseName}</td>
                            <td class="color-white">${index.description}</td>
                            <td class="color-white">${index.enrolledOn}</td>
                            <td class="color-white">${index.status}</td>
                            <td class="color-white">${index.price}</td></tr>
                            `)
                        })
                    }
                })
            })
        }else{
            document.querySelector('#profileContainer').innerHTML = 
                `<div class="col-12 text-center  jumbotron">
                <div class="glassPure adjustAdminProfile">
                <h2 class="color-white mt-5">HELLO THERE ADMIN</h2>
                <h5 class="mt-3 color-white">Name: ${data.firstName} ${data.lastName}</h5>
                <h5 class="color-white">Email: ${data.email}</h5>
                <h5 class="color-white mb-5">Mobile Number: ${data.mobileNo}</h5>
                </div>
                </div>`
            
                 fetch(`https://aem-booking-system.onrender.com/api/users/getAll`, {
                        method:'GET',
                        headers: {
                            'Content-Type': 'application/json', 
                            'Authorization': `Bearer ${token}`
                        }
                }) 
                .then(res => res.json())
                .then( data => {
                    $('#allStudents').append(`
                    <h2 class="color-white text-center mt-3">ALL ENROLLEES</h2>
                    <div style="overflow-x:auto;">
                    <table id="enrollees" class="table adjustEnrollees col-sm-12 col-lg-12 col-md-12 w-100">
                    <tr class="text-center">
                        <th class="color-white">Name</th>
                        <th class="color-white">Email</th>
                        <th class="color-white">Mobile Number</th>
                    </tr>
                    </table>
                    </div>
                    `)
                    let allData = data.sort(function(a, b){
                        var x = a.name.toLowerCase();
                        var y = b.name.toLowerCase();
                        if (x < y) {return -1;}
                        if (x > y) {return 1;}
                        return 0;
                    });
                    allData.forEach( index => {
                        
                        $('#enrollees').append(`
                        <tr>
                            <td class="color-white">${index.name}</td>
                            <td class="color-white">${index.email}</td>
                            <td class="color-white">${index.mobileNo}</td>
                        </tr>
                        `)
                    })
                })
            
        }
    })
}else{
    document.querySelector('#profileContainer').innerHTML = 
    `<div class="col-12 text-center">
    <h2>HELLO THERE VIEWER</h2>
    </div>`
}
