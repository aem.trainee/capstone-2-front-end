let loginForm = document.querySelector('#logInUser')



loginForm.addEventListener('submit', (e) => {
    e.preventDefault()
let email = document.querySelector('#userEmail').value
let password = document.querySelector('#password').value
let gatheredInfo = {
    "email": email,
    "password": password
}

if(email=='' || password==''){
    Qual.errordb('Error', 'Fill up all the fields');
}else{
    fetch('https://aem-booking-system.onrender.com/api/users/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(gatheredInfo)
    })
    .then( res => res.json())
    .then(data => {
        if(data.accessToken != undefined){
            localStorage.setItem('token', data.accessToken);
    
            fetch('https://aem-booking-system.onrender.com/api/users/details', {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${data.accessToken}`
                }
            })
            .then(res => res.json())
            .then(data => {
                localStorage.setItem("id", data._id)
                localStorage.setItem("isAdmin", data.isAdmin)
                // console.log(localStorage)
                Qual.successdb('Success', 'Welcome to Aemzano Booking System')
                let delayInMilliseconds = 2000; //2 second
                setTimeout(function() {
                    window.location.replace('./courses.html')
                }, delayInMilliseconds);
            })
        }else{
            Qual.errordb('NO ACCESS TOKEN RECEIVED', 'USER NOT REGISTERED');
            
        }       
    })
}

})