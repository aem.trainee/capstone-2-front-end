let navItems = document.querySelector('#navSession')



let userToken = localStorage.getItem("token")


if(!userToken){

    $('.navbar-nav').append(`
    <li class="nav-item">
		<a href="./register.html" class="nav-link"> Register </a>
    </li>
    <li class="nav-item">
        <a href="./login.html" class="nav-link">Log in</a>
    </li>
    `)

    
    document.querySelector('#hideNoToken').remove()

}else{
    navItems.innerHTML =  
    `<li class="nav-item">
    <a href="./logout.html" class="nav-link">Log Out</a>
    </li>`
}

