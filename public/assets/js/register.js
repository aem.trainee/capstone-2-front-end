// (USING JQUERY)
// $('button').click(function(){
// let firstName = $('#firstName').val()
// let lastName = $('#lastName').val()
// let mobileNumber = $('#mobileNumber').val()
// let email = $('#userEmail').val()
// let password1 = $('#password1').val()
// let password2 = $('#password2').val()

//     console.log(`firstName: ${firstName}`)
//     console.log(`lastName: ${lastName}`)
//     console.log(`mobileNumber: ${mobileNumber}`)
//     console.log(`email: ${email}`)
//     console.log(`password: ${password1}`)

//     alert('successfully logged in')
// })


// REGISTER UPON CLICKING THE BUTTON
// document.querySelector('#register').onclick = function(){
// let firstName = document.querySelector('#firstName').value
// let lastName = document.querySelector('#lastName').value
// let mobileNumber = document.querySelector('#mobileNumber').value
// let email = document.querySelector('#userEmail').value
// let password1 = document.querySelector('#password1').value
// let password2 = document.querySelector('#password2').value
// let gatheredInfo = {
//     "firstName" : firstName,
//     "lastName": lastName,
//     "mobileNo": mobileNumber,
//     "email": email,
//     "password": password1
// }

// let atpos = email.indexOf("@");
// let dotpos = email.lastIndexOf(".");

//         if(firstName!='' && lastName!='' && mobileNumber!='' && email!='' && password1!='' && password2!=''){
//             if(password1.length<8 || password2.length<8){
//                 alert('Password should be atleast 8 characters');
//             }else if(password1 != password2){
//                 alert('2 passwords should be identical');
//             }else if(atpos < 1 || ( dotpos - atpos < 2 )){
//                 alert('type a valid email');
//             }else if( mobileNumber.length != 11 || mobileNumber[0] != 0 || mobileNumber[1] != 9){
//                 alert('type a valid mobile number: 11 digits starting with 09');
//             }else{
//             //fetch - register
//             fetch('https://aem-booking-system.onrender.com/api/users/', {
//                 method: 'POST',
//                 headers: {
//                     'Content-Type': 'application/json',
//                 },
//                 body: JSON.stringify(gatheredInfo)
//             })
//             .then (data => data.json())
//             .then( dataGathered => {
//                 console.log(dataGathered)
//                 if(dataGathered == true){
//                   alert('SUCCESSFULLY REGISTERED!')  
//                 }else{
//                   alert('NOT SUCCESSFULLY REGISTERED!')  
//                 }
                
//             })
//             .catch(error => {
//                 console.log(error)
//             })   
//             }//end of fetch

//         }else{
//             alert('FILL UP ALL THE FIELDS')
//         }
    
// }

document.querySelector('#userEmail').addEventListener("change", function(){  
    let userEmail = document.querySelector('#userEmail').value    
    fetch('https://aem-booking-system.onrender.com/api/users/email-exists', {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({email: userEmail})
    })
    .then(result => result.json())
    .then(data => {
        if(data === false){
            // alert('EMAIL IS VALID TO USE')
            Qual.successdb('Valid', 'Email is valid to use')
        }else{
            Qual.errordb('Error', 'Email is already is use')
        }
    })
});

let registerForm = document.querySelector('#registerUser')

registerForm.addEventListener('submit', (e) => {
    e.preventDefault()

let firstName = document.querySelector('#firstName').value
let lastName = document.querySelector('#lastName').value
let mobileNumber = document.querySelector('#mobileNumber').value
let userEmail = document.querySelector('#userEmail').value
let password1 = document.querySelector('#password1').value
let password2 = document.querySelector('#password2').value
let registerUser = document.querySelector('#registerUser').value

let gatheredInfo = {
    "firstName" : firstName,
    "lastName": lastName,
    "mobileNo": mobileNumber,
    "email": userEmail,
    "password": password1
}

//VALIDATION
if((password1 !== '' && password2 !== '') && (password1 === password2)
&& (mobileNumber.length === 11)){
    //check database
    //check for duplicate email in database
    fetch('https://aem-booking-system.onrender.com/api/users/email-exists', {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({email: userEmail})
    })
    .then(result => result.json())
    .then(data => {
        //received data from DB if email duplicate or not
        if(data === true){
            Qual.errordb('Error', 'Email is already is use')
        }else if(data === false){
            //fetch register
            fetch('https://aem-booking-system.onrender.com/api/users/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(gatheredInfo)
            })
            .then(res => res.json())
            .then(data => {
                if(data === true){
                    // alert('REGISTERED SUCCESSFULLY')
                    // window.location.replace('./login.html')
                    Qual.successdb('Success', 'Registered successfully')
                    let delayInMilliseconds = 2000; //2 second
                    setTimeout(function() {
                        window.location.replace('./login.html')
                    }, delayInMilliseconds);
                }else(
                    alert('NOT REGISTERED SUCCESSFULLY')
                )
               
            })
        }
    })
}else{
    Qual.errordb('Error', 'Fill up all the fields')
}

})




        
    
